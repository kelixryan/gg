<%-- 
    Document   : home
    Created on : Mar 14, 2019, 4:40:33 PM
    Author     : brian
--%>
<%@page import="java.sql.*"  %>
<%@page import="javax.swing.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="lib/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
         <script>
            function confirmGo(m,u) {
                if ( confirm(m) ) {
                    window.location = u;
                }
            }
        </script> </head>
    <body class="container">
        <div class="row">
            <div class="col-md-12" style="height: 3em;"></div>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h3 class="input-group-text">Hello thank you for registering!</h3>
                <table class="table" id="editable">
                <%
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/uganda","root","root");
                    String query="select * from person";
                    PreparedStatement pst = con.prepareStatement(query);
                    ResultSet rs = pst.executeQuery();
                    while(rs.next())
                    {
                        %>
                        <tr>
        <td><%=rs.getString("firstname") %></td>      <td><%=rs.getString("lastname")%></td>   <td><%=rs.getString("email")%></td>
                    <td><a href="update.jsp?id=<%=rs.getString("email")%>">Update</a></td>
                   <td><a href="javascript:confirmGo('Sure to delete this record?','deletedb.jsp?id=<%=rs.getString("email")%>')">Delete</a></td>                        
                        </tr>
                <%
                    }
                %>
                </table>
            </div>
            <div class="col-md-3"></div>
        </div>
    </body></html>
