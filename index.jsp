<%-- 
    Document   : index
    Created on : Mar 12, 2019, 5:57:06 PM
    Author     : brian
--%>
<%@page import="java.sql.*"  %>
<%@page import="javax.swing.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="lib/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="container">
        <div class="row">
            <div class="col-md-12" style="height: 3em;"></div>  
            <div class="col-md-4">
                <img src="img/users.png" alt=""/>              
            </div>
        <div class="col-md-4">
        <h1>Hello World!</h1>
        <form method="post" action="process.jsp">
            FirstName:<input type="text" size="15" name="fname" class="form-control" /><br/>
            LastName:<input type="text" size="15" name="lname" class="form-control" /><br/>
            email:<input type="text" size="15" name="email" class="form-control" /><br/>
            Password:<input type="password" size="15" name="pwd" class="form-control" /><br/>
            <input type="submit" value="Register" class="btn-primary" />
            <input type="reset" value="Cancel" class="btn-danger" />
        </form>
        </div>
        <div class="col-md-4">
            <a href="home.jsp" class="navbar-text">Welcome page</a>
        </div>
        </div>
    </body>
</html>
